package com.dex.runner.screens;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.dex.runner.screens.tween.SpriteAsccessor;

public class level  implements Screen{
	
	Texture back;
	private Box2DDebugRenderer debugRenderer;
	private SpriteBatch batch;
	private Sprite splash;
	private TweenManager tweenManager;
	private World world;
	private OrthographicCamera camera;
	Texture texture_back;
	Sprite sprite_back;
	Music mainMusic;
	SpriteBatch spriteBatch;
	Texture spriteTexture;
	Sprite sprite;
	float scrollTimer = 3f;
	
	
	@Override
	public void render(float delta) {
		 Gdx.gl.glClearColor(0, 0, 0, 1);
		 Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	     
		 debugRenderer.render(world, camera.combined);
		 
	     sprite.setU(scrollTimer);
	     sprite.setU2((float) (scrollTimer+1));
		 scrollTimer+=0.0055f;
	                
	     
	     if(scrollTimer>1.0f)
	         scrollTimer = 0.0f;    
	     
	     tweenManager.update(delta);
	     
	     
	     spriteBatch.begin();
	        spriteBatch.draw(back, 0, 0);
	     	sprite.draw(spriteBatch);
	     spriteBatch.end();
	     
	     
	 }

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		
		world = new World(new Vector2(0, -9.81f), true);
		debugRenderer = new Box2DDebugRenderer();
		
		camera = new OrthographicCamera(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		
		spriteBatch = new SpriteBatch();
		tweenManager = new TweenManager();
		Tween.registerAccessor(Sprite.class, new SpriteAsccessor());
	    spriteTexture = new Texture(Gdx.files.internal("img/ground.png"));
	                 
	    spriteTexture.setWrap(TextureWrap.Repeat,TextureWrap.Repeat);
	    sprite = new Sprite(spriteTexture, 0, 0, 1280, 100);
	    sprite.setSize(1280, 100);
	    
	    back = new Texture(Gdx.files.internal("img/background.png"));
	     
		
		Tween.set(sprite, SpriteAsccessor.ALPHA).target(0).start(tweenManager);
		Tween.to(sprite, SpriteAsccessor.ALPHA, 2). target(1).start(tweenManager);
			
		
	}

	@Override
	public void hide() {

		
	}

	@Override
	public void pause() {

		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		batch.dispose();
		splash.getTexture().dispose();
		
	}

}
