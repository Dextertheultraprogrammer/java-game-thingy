package com.dex.runner;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.dex.runner.screens.splash;

public class Runnergame extends Game  {
	
	public static final String TITLE = "Runner", VERSION = "0.1 DEV BUILD";
	
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Sprite sprite;
	
	@Override
	public void create() {	
		Texture.setEnforcePotImages(false);
		setScreen(new splash());
		
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public void render() {		
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
